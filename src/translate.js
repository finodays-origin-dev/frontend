export const emotionTranslate = {
  anger: "Злой",
  disgust: "Отвращение",
  fear: "Страх",
  happiness: "Счастливый",
  sadness: "Грустный",
  surprise: "Удивлённый",
  neutral: "Нейтральный",
  contempt:"Презрение"
};
