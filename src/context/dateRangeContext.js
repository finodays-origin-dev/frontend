import React from "react";

const reducer = (state, action) => {
  switch (action.type) {
    case "start":
      return {
        ...state,
        start: action.payload,
      };
    case "end":
      return {
        ...state,
        end: action.payload,
      };
    default:
      return state;
  }
};
const beforeDate = () => {
  let d = new Date();
  d.setDate(d.getDate() - 1);
  return d;
};
const nowPlusOne = () => {
  let d = new Date();
  d.setDate(d.getDate() + 1 );
  return d;
};
export const initialState = {
  start: beforeDate(),
  end: nowPlusOne(),
};
export const DateRangeContext = React.createContext({
  state: initialState,
  dispatch: () => null,
});

export const DateRangeProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return (
    <DateRangeContext.Provider value={[state, dispatch]}>
      {children}
    </DateRangeContext.Provider>
  );
};
