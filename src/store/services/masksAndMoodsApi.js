import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const initialStateWithValues = {
  allAndMasks: [
    {
      name: "12:00",
      all: 30,
      masks: 8,
    },
    {
      name: "13:00",
      all: 20,
      masks: 7,
    },
    {
      name: "14:00",
      all: 30,
      masks: 8,
    },
    {
      name: "15:00",
      all: 30,
      masks: 8,
    },
    {
      name: "16:00",
      all: 30,
      masks: 8,
    },
    {
      name: "17:00",
      all: 27,
      masks: 6,
    },
    {
      name: "18:00",
      all: 40,
      masks: 10,
    },
  ],
  mooods: [
    { name: "Счастливый", value: 500 },
    { name: "Злой", value: 300 },
    { name: "Грустный", value: 300 },
    { name: "Не распознано", value: 100 },
  ],
};

const initialState = {
  allAndMasks: [],
  mooods: [],
};

export const masksAndMoodsApi = createApi({
  reducerPath: "masksAndMoods",
  baseQuery: fetchBaseQuery(
    { baseUrl: "http://finodays.dchudinov.ru/api/" },
    initialState
  ),
  endpoints: (builder) => ({
    getMasksAndAll: builder.query({
      query: ({start, end}) => `stats/masks?start=${start}&end=${end}`,
    }),
    getMoods: builder.query({
      query: ({start, end}) => `stats/moods?start=${start}&end=${end}`,
    }),
  }),
});

// Action creators are generated for each case reducer function
export const { useGetMasksAndAllQuery, useGetMoodsQuery } = masksAndMoodsApi;
