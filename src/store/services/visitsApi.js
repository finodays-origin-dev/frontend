import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const initialState = {
  visits: [],
};

export const visitsApi = createApi({
  reducerPath: "visits",
  baseQuery: fetchBaseQuery(
    { baseUrl: "http://finodays.dchudinov.ru/api/" },
    initialState
  ),
  endpoints: (builder) => ({
    getVisits: builder.query({
      query: ({start, end}) => `visits?start=${start}&end=${end}`,
    }),
    getVisitByID: builder.query({
      query: (id) => `visits/${id}`,
    }),
  }),
});

// Action creators are generated for each case reducer function
export const { useGetVisitByIDQuery, useGetVisitsQuery } = visitsApi;
