import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { masksAndMoodsApi } from "./services/masksAndMoodsApi";
import { visitsApi } from "./services/visitsApi";
import dateRange from "./features/dateRangeSlice";
import selectedVisit from "./features/selectedVisitSlice"
export const store = configureStore({
  reducer: {
    [masksAndMoodsApi.reducerPath]: masksAndMoodsApi.reducer,
    [visitsApi.reducerPath]: visitsApi.reducer,
    dateRange,
    selectedVisit
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      // serializableCheck: false,
    }).concat(masksAndMoodsApi.middleware)
      .concat(visitsApi.middleware),
});

setupListeners(store.dispatch);
