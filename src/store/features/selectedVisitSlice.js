import { createSlice } from "@reduxjs/toolkit";

export const selectedVisitSlice = createSlice({
  name: "selectedVisit",
  initialState: {
    selectedVisit: 0,
  },
  reducers: {
    setSelectedVisit: (state, action) => {
      state.selectedVisit = action.payload;
    },
  },
});

export const { setSelectedVisit } = selectedVisitSlice.actions;
export default selectedVisitSlice.reducer;
