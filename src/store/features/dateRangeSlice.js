import { createSlice } from "@reduxjs/toolkit";

const getCurrentDateString = () => {
  const date = new Date();
  // date.setDate(date.getDate() );
  return date.toISOString();
};
const getDayBefore = () => {
  const date = new Date();
  date.setDate(date.getDate() - 1);
  return date.toISOString();
};

export const dateRangeSlice = createSlice({
  name: "dateRange",
  initialState: {
    start: getDayBefore(),
    end: getCurrentDateString(),
    currentType: "day",
  },
  reducers: {
    setStart: (state, action) => {
      state.start = new Date(action.payload).toISOString();
    },
    setEnd: (state, action) => {
      state.end = new Date(action.payload).toISOString();
    },
    setCurrentType: (state, action) => {
      state.currentType = action.payload;
      // const date = new Date();
      // state.end = getCurrentDateString();
      // switch (action.payload) {
      //   case "day":
      //     date.setDate(date.getDate() - 1);
      //     state.start = JSON.parse(date);
      //     console.log(state.start)
      //     break;
      //   case "week":
      //     date.setDate(date.getDate() - 7);
      //     console.log(date);
      //     state.start = JSON.parse(date);
      //     break;
      //   case "month":
      //     date.setDate(date.getDate() - 30);
      //     console.log(date);
      //     state.start = JSON.parse(date);
      //     break;
      //   default:
      //     // console.log("not correct type" + action.payload);
      //     break;
      // }
      // let dateJson = JSON.stringify(date)
      // state.start = JSON.Parse(dateJson);
    },
  },
});

export const { setStart, setEnd, setCurrentType } = dateRangeSlice.actions;
export default dateRangeSlice.reducer;
