import React, { useEffect, useState } from "react";
import { ButtonGroup, Button, TextField } from "@mui/material";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { DateRangeContext } from "../../context/dateRangeContext";
const getDayBefore = () => {
  const date = new Date();
  date.setDate(date.getDate() - 1);
  return date;
};

const nowPlusOne = () => {
  let d = new Date();
  d.setDate(d.getDate() + 1);
  return d;
};
const DateSelector = () => {
  const [state, dispatch] = React.useContext(DateRangeContext);
  // const [start, setStart] = useState(state.start);
  // const [end, setEnd] = useState(state.end);
  const [type, setType] = useState("day");
  const handleChangeStart = (newValue) => {
    // setStart(newValue);
    dispatch({ type: "start", payload: newValue });
  };
  const handleChangeEnd = (newValue) => {
    // console.log(newValue);
    // setEnd(newValue);
    dispatch({ type: "end", payload: newValue });
  };
  useEffect(() => {
    let date = new Date();
    dispatch({ type: "end", payload: nowPlusOne() });
    switch (type) {
      case "day":
        date.setDate(date.getDate() - 1);
        break;
      case "week":
        date.setDate(date.getDate() - 7);
        break;
      case "month":
        date.setDate(date.getDate() - 30);
        break;
      default:
        date.setDate(date.getDate() - 1);
        // console.log("not correct type" + action.payload);
        break;
    }
    // console.log(date);
    dispatch({ type: "start", payload: date });
  }, [type]);
  return (
    <div>
      <ButtonGroup
        variant="outlined"
        aria-label="outlined primary button group"
      >
        <Button
          variant={type === "day" ? "contained" : "outlined"}
          onClick={() => setType("day")}
        >
          День
        </Button>
        <Button
          variant={type === "week" ? "contained" : "outlined"}
          onClick={() => setType("week")}
        >
          Неделя
        </Button>
        <Button
          variant={type === "month" ? "contained" : "outlined"}
          onClick={() => setType("month")}
        >
          Месяц
        </Button>
        <Button
          variant={type === "other" ? "contained" : "outlined"}
          onClick={() => setType("other")}
        >
          Другое
        </Button>
      </ButtonGroup>
      <div
        style={{
          display: type === "other" ? "flex" : "none",
        }}
      >
        <div style={{ marginTop: "10px", display: "flex" }}>
          {/* <p>От</p> */}
          <DesktopDatePicker
            label="Дата начала"
            inputFormat="yyyy-MM-dd"
            value={state.start}
            onChange={handleChangeStart}
            renderInput={(params) => <TextField {...params} />}
          />
        </div>
        <div style={{ marginTop: "10px", marginLeft: "15px", display: "flex" }}>
          {/* <p>До</p> */}
          <DesktopDatePicker
            label="Дата конца"
            inputFormat="yyyy-MM-dd"
            value={state.end}
            onChange={handleChangeEnd}
            renderInput={(params) => <TextField {...params} />}
          />
        </div>
      </div>
    </div>
  );
};

export default DateSelector;
