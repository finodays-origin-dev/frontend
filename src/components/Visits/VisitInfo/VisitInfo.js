import React from "react";
import PersonImg from "../../../static/person.jpg";
import PersonWithMaskImg from "../../../static/WithMask.webp";
import PersonWithoutMaskImg from "../../../static/WithoutMask.webp";
import { emotionTranslate } from "../../../translate";

const VisitInfo = (props) => {
  return (
    <div style={{maxWidth:"100%", width:"100%"}}>
      <h2>ID визита: {props?.client?.ID}</h2>
      <div
        className="visit-info"
        // style={{ display: "flex", justifyContent: "space-around" }}
      >
        <div>
          {/* <img src={"data:image/png;base64,"+props?.client?.image} alt="person" style={{ width: 300, height: 400 }} /> */}
          <img
            src={
              props?.client?.mask_status
                ? PersonWithMaskImg
                : PersonWithoutMaskImg
            }
            alt="person"
            className="person-avatar"
            // style={{ width: "80%", height: "90%" }}
          />
        </div>
        <div className="visiter-discription" style={{ width: "95%",  marginLeft:"10px" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <p style={{ fontWeight: "bold" }}>Дата: </p>
            <p>{props?.client?.CreatedAt?.split("T")[0]}</p>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <p style={{ fontWeight: "bold" }}>Время:</p>{" "}
            <p>{props?.client?.CreatedAt?.split("T")[1]?.split(".")[0]}</p>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <p style={{ fontWeight: "bold" }}>Маска: </p>
            {props?.client?.mask_status ? "Присутствует" : "Отсутствует"}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <p style={{ fontWeight: "bold" }}>Настроение: </p>
            <p>{emotionTranslate[props?.client?.emotion]}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VisitInfo;
