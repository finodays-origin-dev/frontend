import React from "react";
import { IconButton, ListItem, List, ListItemText } from "@mui/material";
import CommentIcon from "@mui/icons-material/Comment";
import { FixedSizeList } from "react-window";
import { useDispatch } from "react-redux";
import { setSelectedVisit } from "../../../store/features/selectedVisitSlice";

const People = (data, index) => {
  const dispatch = useDispatch();
  return (
    <ListItem
      style={{ width: "100%" }}
      key={index}
      component="div"
      disablePadding
    >
      <ListItemText
        onClick={() => dispatch(setSelectedVisit(data?.index))}
        primary={
          <p className="id-visit" style={{  }}>
            {"ID визита: " + data?.data[data?.index]?.ID}
          </p>
        }
        style={{
          textAlign: "center",
          border: "1px solid black",
          margin: "5px 10px",
          padding: "5px 10px",
        }}
        // style={{fontSize: "14px"}}
      />
    </ListItem>
  );
};

const VisitsList = (props) => {
  return (
    <div>
      {/* <List
        sx={{
          width: "200px",
          height: 300,
          maxWidth: 360,
          bgcolor: "background.paper",
        }}
      > */}
      {/* {Array.from(Array(30).keys()).map((value,index) => ( */}
      <div>
        <FixedSizeList
          height={800}
          // width={300}
          itemSize={46}
          itemCount={props?.visits?.length}
          // overscanCount={5}
          itemData={props?.visits}
          className="fixed-size-list"
        >
          {People}
        </FixedSizeList>
      </div>
      {/* ))} */}
      {/* </List> */}
    </div>
  );
};

export default VisitsList;
