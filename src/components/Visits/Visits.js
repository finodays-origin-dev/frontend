import React, { useEffect } from "react";
import VisitInfo from "./VisitInfo";
import VisitsList from "./VisitsList";
import { useGetVisitsQuery } from "../../store/services/visitsApi";
import { DateRangeContext } from "../../context/dateRangeContext";
import { useSelector } from "react-redux";
import "./Visit.css";

const Visits = () => {
  const selectedVisit = useSelector((state) => state.selectedVisit);
  const [state, dispatch] = React.useContext(DateRangeContext);
  const { data, isError, isLoading, refetch } = useGetVisitsQuery({
    start: state.start?.toISOString().split("T")[0],
    end: state.end.toISOString().split("T")[0],
  });
  useEffect(() => {
    // console.log(refetchMasks);
    refetch({
      start: state.start?.toISOString().split("T")[0],
      end: state.end.toISOString().split("T")[0],
    });
  }, [state.start, state.end]);
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "flex-start",
        width: "100%",
        maxWidth: "100%",
      }}
    >
      <div
        // style={{ borderRight: "2px solid black", width: "300px" }}
        className="visit-list-main"
      >
        <VisitsList visits={data?.result} />
      </div>
      <div className="visit-info-main">
        <VisitInfo client={data?.result[selectedVisit?.selectedVisit]} />
      </div>
    </div>
  );
};

export default Visits;
