import React from "react";
import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from "recharts";
import { emotionTranslate } from "../../../translate";
// const data = [
//   { name: "Счастливый", value: 500 },
//   { name: "Злой", value: 300 },
//   { name: "Грустный", value: 300 },
//   { name: "Не распознано", value: 100 },
// ];

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042", "green", "purple", "pink", "red"];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index,
  data,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
      x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? "start" : "end"}
      dominantBaseline="central"
    >
      {/* {`${(percent * 100).toFixed(0)}%`} */}
      {data[index] ? data[index].name : null}
    </text>
  );
};

const PeoplesAndMasks = (props) => {
  if (props.isError) {
    return <h3>Произошла ошибка, попробуйте позже...</h3>;
  }
  if (props.isLoading) {
    return <h3>Идёт згрузка...</h3>;
  }
  return (
    // <div style={{ width: "600px" }}>
    <div style={{ width: "98%",  }}>
      <h3 style={{fontSize:"22px", marginBottom:"20px"}}>Настроение посетителей</h3>
      <div 
      // style={{ display: "flex", justifyContent: "space-around" }}
      className="moods-chart"
      >
      <ResponsiveContainer className="response-container-moods" height={305}>
        <PieChart >
          <Pie
            data={props?.data}
            cx="50%"
            cy="50%"
            labelLine={false}
            // label={renderCustomizedLabel}
            outerRadius={150}
            fill="#8884d8"
            dataKey="value"
          >
            {props?.data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index]} />
            ))}
          </Pie>
        </PieChart>
        </ResponsiveContainer>
        <div>
          {props?.data?.map((d, index) => (
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                fontSize: "18px",
              }}
            >
              <p style={{margin:"10px 0"}}>{emotionTranslate[d.name]}</p>{" "}
              <div
                style={{
                  width: 25,
                  height: 25,
                  backgroundColor: COLORS[index],
                  marginLeft: "15px",
                }}
              ></div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
export default PeoplesAndMasks;
