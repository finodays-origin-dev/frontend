import React, { useState } from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import Checkbox from "@mui/material/Checkbox";

const data = [
  {
    name: "12:00",
    all: 30,
    masks: 8,
  },
  {
    name: "13:00",
    all: 20,
    masks: 7,
  },
  {
    name: "14:00",
    all: 30,
    masks: 8,
  },
  {
    name: "15:00",
    all: 30,
    masks: 8,
  },
  {
    name: "16:00",
    all: 30,
    masks: 8,
  },
  {
    name: "17:00",
    all: 27,
    masks: 6,
  },
  {
    name: "18:00",
    all: 40,
    masks: 10,
  },
];

const PeoplesAndMasks = (props) => {
  const [isShowWrong, setIsShowWrong] = useState(false);
  // console.log("Props", props)
  if (props?.isError) {
    return <h3>Произошла ошибка, попробуйте позже...</h3>;
  }
  if (props?.isLoading) {
    return <h3>Идёт згрузка...</h3>;
  }
  return (
    // <div style={{ width: "800px" }}>
    <div style={{ width: "100%" }}>
      <h3 style={{ fontSize: "22px" }}>Количество поситителей в масках</h3>
      <div
        style={{
          margin: "10 auto",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <div className="wrong-masks" >
          Отображать кол-во людей с неправильно надетыми масками
        </div>
        <div>
          <Checkbox
            checked={isShowWrong}
            onChange={() => setIsShowWrong(!isShowWrong)}
          />
        </div>
      </div>
      <ResponsiveContainer width="95%" height={400}>
        <AreaChart
          // title="Количество поситителей"
          // width={800}
          // height={400}
          data={props?.data ?? []}
          margin={{
            top: 10,
            right: 30,
            left: 0,
            bottom: 0,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Area
            type="monotone"
            name="В маске"
            dataKey="none"
            stackId="2"
            stroke="purple"
            fill="purple"
          />
          <Area
            type="monotone"
            name="Всего"
            dataKey="all"
            stackId="1"
            stroke="green"
            fill="green"
          />
          {/* <Area
          type="monotone"
          name="Неправильно надетая маска"
          dataKey="wrong"
          stackId="3"
          stroke="blue"
          fill="blue"
          display={isShowWrong ? "block" : "none"}
        /> */}
        </AreaChart>
      </ResponsiveContainer>
    </div>
  );
};
export default PeoplesAndMasks;
