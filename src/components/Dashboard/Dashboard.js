import React, { useEffect } from "react";
import PeoplesAndMasks from "./PeoplesAndMasks";
import PeoplesAndMoods from "./PeoplesAndMoods";
import {
  useGetMasksAndAllQuery,
  useGetMoodsQuery,
} from "../../store/services/masksAndMoodsApi";
import { DateRangeContext } from "../../context/dateRangeContext";
import "./Dashboard.css";

const Dashboard = (props) => {
  const [state, dispatch] = React.useContext(DateRangeContext);
  const {
    data: dataMask,
    error: errorMasksAndAll,
    isLoading: isLoadingMasksAndAll,
    refetch: refetchMasks,
  } = useGetMasksAndAllQuery(
    {
      start: state.start?.toISOString().split("T")[0],
      end: state.end.toISOString().split("T")[0],
    },
    {
      // pollingInterval: 6000,
    }
  );
  const {
    data: moodsAndAll,
    error: errorMoodsAndAll,
    isLoading: isLoadingMoodsAndAll,
    refetch: refetchMoods,
  } = useGetMoodsQuery(
    {
      start: state.start?.toISOString().split("T")[0],
      end: state.end.toISOString().split("T")[0],
    },
    {
      // pollingInterval: 6000,
    }
  );
  useEffect(() => {
    // console.log(refetchMasks);
    refetchMoods({
      start: state.start?.toISOString().split("T")[0],
      end: state.end.toISOString().split("T")[0],
    });
    refetchMasks({
      start: state.start?.toISOString().split("T")[0],
      end: state.end.toISOString().split("T")[0],
    });
    console.log(dataMask);
  }, [state.start, state.end]);

  // console.log(masksAndAll);
  return (
    <div
      // style={{ display: "flex", justifyContent: "space-around" }}
      className={"stats"}
    >
      <PeoplesAndMasks
        data={dataMask?.result}
        isError={errorMasksAndAll}
        isLoading={isLoadingMasksAndAll}
      />
      <PeoplesAndMoods
        data={moodsAndAll?.result}
        isError={errorMoodsAndAll}
        isLoading={isLoadingMoodsAndAll}
      />
    </div>
  );
};

export default Dashboard;
