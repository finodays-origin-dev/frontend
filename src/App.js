import logo from "./logo.svg";
import "./App.css";
import NavigationComponent from "./components/Navigation";
import DateSelector from "./components/DateSelector/DateSelector";
import { DateRangeProvider } from "./context/dateRangeContext";

function App() {
  return (
    <DateRangeProvider>
      <>
        <div className="App">
          <header>
            <h1>Админ панель для мониторинга гостей банка</h1>
            <div
              className="date-description"
              // style={{
              //   display: "flex",
              //   justifyContent: "center",
              //   alignItems: "center",
              //   alignSelf: "center",
              //   margin: "0 auto",
              //   // width: "750px",
              // }}
            >
              <div className="date-description-title">
                Отображение за период
              </div>
              <div 
              className="date-description-selector"
              // style={{ marginLeft: "20px", marginTop:"10px" }}
              >
                <DateSelector />
              </div>
            </div>
          </header>
          <NavigationComponent />
        </div>
      </>
    </DateRangeProvider>
  );
}

export default App;
